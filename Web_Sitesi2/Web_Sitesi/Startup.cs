﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Web_Sitesi.Startup))]
namespace Web_Sitesi
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
