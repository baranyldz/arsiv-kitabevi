﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Web_Sitesi.Models;

namespace Web_Sitesi.Controllers
{
    public class AdminController : Controller
    {
        private KayitİslemDBEntities2 db = new KayitİslemDBEntities2();

        // GET: tabloes
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult KitapIndex()
        {
            return View(db.tablo.ToList());
        }
        // GET: tabloes/Details/5
        public ActionResult KitapDetails(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tablo tablo = db.tablo.Find(id);
            if (tablo == null)
            {
                return HttpNotFound();
            }
            return View(tablo);
        }

        // GET: tabloes/Create
        public ActionResult KitapCreate()
        {
            return View();
        }

        // POST: tabloes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult KitapCreate([Bind(Include = "UserID,Ad,email,kadi,sifre,sifre_tekrar")] tablo tablo)
        {
            if (ModelState.IsValid)
            {
                db.tablo.Add(tablo);
                db.SaveChanges();
                return RedirectToAction("KitapIndex");
            }

            return View(tablo);
        }

        // GET: tabloes/Edit/5
        public ActionResult KitapEdit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tablo tablo = db.tablo.Find(id);
            if (tablo == null)
            {
                return HttpNotFound();
            }
            return View(tablo);
        }

        // POST: tabloes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult KitapEdit([Bind(Include = "UserID,Ad,email,kadi,sifre,sifre_tekrar")] tablo tablo)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tablo).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("KitapIndex");
            }
            return View(tablo);
        }

        // GET: tabloes/Delete/5
        public ActionResult KitapDelete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tablo tablo = db.tablo.Find(id);
            if (tablo == null)
            {
                return HttpNotFound();
            }
            return View(tablo);
        }

        // POST: tabloes/Delete/5
        [HttpPost, ActionName("KitapDelete")]
        [ValidateAntiForgeryToken]
        public ActionResult KitapDeleteConfirmed(int id)
        {
            tablo tablo = db.tablo.Find(id);
            db.tablo.Remove(tablo);
            db.SaveChanges();
            return RedirectToAction("KitapIndex");
        }

        //KİTAP EKLEME CİKARMA//KİTAP EKLEME CİKARMA//KİTAP EKLEME CİKARMA//KİTAP EKLEME CİKARMA//KİTAP EKLEME CİKARMA
        public ActionResult Kitap2Index()
        {
            return View(db.tablo2.ToList());
        }
        // GET: tabloes/Details/5
        public ActionResult Kitap2Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tablo2 tablo2 = db.tablo2.Find(id);
            if (tablo2 == null)
            {
                return HttpNotFound();
            }
            return View(tablo2);
        }

        // GET: tabloes/Create
        public ActionResult Kitap2Create()
        {
            return View();
        }

        // POST: tabloes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Kitap2Create([Bind(Include = "resim_id,resim,icerik,yorum")] tablo2 tablo2)
        {
            if (ModelState.IsValid)
            {
                db.tablo2.Add(tablo2);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tablo2);
        }

        // GET: tabloes/Edit/5
        public ActionResult Kitap2Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tablo2 tablo2 = db.tablo2.Find(id);
            if (tablo2 == null)
            {
                return HttpNotFound();
            }
            return View(tablo2);
        }

        // POST: tabloes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult KitapEdit2([Bind(Include = "resim_id,resim,icerik,yorum")] tablo2 tablo2)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tablo2).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Kitap2Index");
            }
            return View(tablo2);
        }

        // GET: tabloes/Delete/5
        public ActionResult Kitap2Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tablo2 tablo2 = db.tablo2.Find(id);
            if (tablo2 == null)
            {
                return HttpNotFound();
            }
            return View(tablo2);
        }

        // POST: tabloes/Delete/5
        [HttpPost, ActionName("Kitap2Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult Kitap2DeleteConfirmed(int id)
        {
            tablo2 tablo2 = db.tablo2.Find(id);
            db.tablo2.Remove(tablo2);
            db.SaveChanges();
            return RedirectToAction("Kitap2Index");
        }
    }
}
