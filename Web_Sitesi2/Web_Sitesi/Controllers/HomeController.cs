﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web_Sitesi.Models;

namespace Web_Sitesi.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            Kitap o1 = new Kitap();
            string anasayfa = "anasayfa";
            o1.kitabim = dbc.tablo2.Where(x=>x.kategori.ToUpper()==anasayfa.ToUpper()).ToList();
            return View(o1);
        }
        public ActionResult İletisim()
        {
            return View();
        }
        [HttpPost]
        public ActionResult İletisim(FormCollection form)
        {
            KayitİslemDBEntities2 db = new KayitİslemDBEntities2();
            iletisim model = new iletisim();
            model.ad = form["ad"].Trim();
            model.e_mail = form["email"].Trim();
            model.konu = form["konu"].Trim();
            model.mesaj = form["mesaj"].Trim();
            db.iletisim.Add(model);
            db.SaveChanges();
            return View();
        }
        public ActionResult Yeni_Kitap()
        {
            Kitap o1 = new Kitap();
            o1.kitabim = dbc.tablo2.ToList();
            return View(o1);
        }
        public ActionResult Kitap_icerik(int ? id)
        {
            tablo2 kitabim = dbc.tablo2.Find(id);
            return View(kitabim);
        }
        public ActionResult Hakkımızda()
        {
            return View();
        }
        public ActionResult Giris_Yap()
        {
            return View();
        }
        public ActionResult Tarih()
        {
            Kitap o1 = new Kitap();
            string tarih = "tarih";
            o1.kitabim = dbc.tablo2.Where(x => x.kategori.ToUpper() == tarih.ToUpper()).ToList();
            return View(o1);
        }
        public ActionResult Bilim()
        {
            Kitap o1 = new Kitap();
            string bilim = "bilim";
            o1.kitabim = dbc.tablo2.Where(x => x.kategori.ToUpper() == bilim.ToUpper()).ToList();
            return View(o1);
        }
        public ActionResult Macera()
        {
            Kitap o1 = new Kitap();
            string macera = "macera";
            o1.kitabim = dbc.tablo2.Where(x => x.kategori.ToUpper() == macera.ToUpper()).ToList();
            return View(o1);
        }
        public ActionResult Polisiye()
        {
            Kitap o1 = new Kitap();
            string polisiye = "polisiye";
            o1.kitabim = dbc.tablo2.Where(x => x.kategori.ToUpper() == polisiye.ToUpper()).ToList();
            return View(o1);
        }
        public ActionResult Korku()
        {
            Kitap o1 = new Kitap();
            string korku = "korku";
            o1.kitabim = dbc.tablo2.Where(x => x.kategori.ToUpper() == korku.ToUpper()).ToList();
            return View(o1);
        }
        public ActionResult Populer()
        {

            Kitap o1 = new Kitap();
            string populer = "popüler";
            o1.kitabim = dbc.tablo2.Where(x => x.kategori.ToUpper() == populer.ToUpper()).ToList();
            return View(o1);
        }
        public ActionResult Cikis()
        {
            Session["kadi"] = null;
            return RedirectToAction("Index", "Home");
        }
         private KayitİslemDBEntities2 dbc = new KayitİslemDBEntities2();
        public ActionResult Giris_Yap2()
        {
            return View();
        }
        public ActionResult Dil()
        {
            return View();
        }
        public ActionResult Admin_Sayfasi()
        {
            return View();
        }
        public ActionResult Firsatlar()
        {
            Kitap o1 = new Kitap();
            o1.kitabim = dbc.tablo2.ToList();
            return View(o1);
        }
        public ActionResult Kayit_Ol()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Kayit_Ol(FormCollection form)
        {
            KayitİslemDBEntities2 db = new KayitİslemDBEntities2();
            tablo model = new tablo();
            model.Ad = form["ad"].Trim();
            model.email = form["email"].Trim();
            model.kadi = form["kullanici_adi"].Trim();
            model.sifre = form["sifre"].Trim();
            model.sifre_tekrar = form["sifre_tekrar"].Trim();
            if (model.sifre!=model.sifre_tekrar)
            {
                Response.Write("<script>alert('Sifreler Uyuşmuyor')</script>");
                return View();
            }
                db.tablo.Add(model);
                db.SaveChanges();
                return View();
        }
        [HttpPost]
        public ActionResult Giris_Yap2(tablo Model, FormCollection form)
        {
            var KULLANICI = dbc.tablo.FirstOrDefault(x=>x.kadi==Model.kadi&&x.sifre==Model.sifre);
            string kullanici_adi = form["kadi"].Trim();
            string sifre = form["sifre"].Trim();
            if (kullanici_adi =="asd" && sifre =="asd")
            {
                Session["Admin"] = "1";
                return RedirectToAction("Admin_Sayfasi","Home");
            }
            else if(KULLANICI!=null)
            {
                Session["kadi"] = KULLANICI;
                return RedirectToAction("Index", "Home");
            }
            Response.Write("<script>alert('Kullanıcı Adını veya Sifreyi Yanlis Girdiniz')</script>");
            return View();
        }
        public class Kitap
        {
        public List<tablo2> kitabim { get; set; }
        }
    }
}